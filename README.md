# Author
Paul Sebastian Hornnes Holand

## What this application is
Java console application for the Noroff course

## Features
You should be able to select a character that could be a mage, warrior, ranger og rogue. Attributes should change if you change class. You should be able to level up the character and equip items that is suited to the character class.

##Issues
The assignment was not finished. I ran into problems regarding being able to figure out how to show items in the characters. Could not figure out how to do this.
Also the testing is not present since I had issues with getting the application to work.
Some pointers on how to make this application to work is appreciated.

## Install
Install IntelliJ
Install JDK 17
Clone repo

