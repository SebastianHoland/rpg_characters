package no.noroff.accelerate.items;

import no.noroff.accelerate.attributes.PrimaryAttributes;

public class Armor extends Item {
    private ArmorType type;
    private PrimaryAttributes primaryAttributes;

    public enum ArmorType {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    public void ArmorType(String name, Integer requiredLevel, Slot slot, ArmorType type, PrimaryAttributes primaryAttributes){
        this.setName(name);
        this.setRequiredLevel(requiredLevel);
        this.type = type;
        this.setSlot(slot);
        this.primaryAttributes = primaryAttributes;

    }

    public ArmorType getType() {
        return type;
    }

    public void setType(ArmorType type) {
        this.type = type;
    }


}
