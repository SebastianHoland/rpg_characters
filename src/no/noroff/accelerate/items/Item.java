package no.noroff.accelerate.items;

public abstract class Item {

    private String name;
    private Integer requiredLevel;
    private Slot slot;

    public enum Slot {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
}
