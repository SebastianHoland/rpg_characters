package no.noroff.accelerate.items;

public class Weapon extends Item {
    private WeaponType type;
    private int damage;
    private double damagePerSecond;
    private double attackSpeed;

    // All the weapons that a character can equip
    public enum WeaponType {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }

    public Weapon(String name, Slot slot, WeaponType type, int damage, double attackSpeed) {
        this.setName(name);
        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.damagePerSecond = damage * attackSpeed;
        this.setSlot(Slot.WEAPON);
    }

    public WeaponType getType() {
        return type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getDamagePerSecond() {
        return damagePerSecond;
    }

    public void setDamagePerSecond(double damagePerSecond) {
        this.damagePerSecond = damagePerSecond;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }
}
